<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends RestApi_Controller 
{
    function __construct() 
    {
        parent::__construct();
        $this->load->library('api_auth');
        $this->load->model('api_model');
    }

    function login() 
    {
        $requestStatus = 0;
        foreach($this->input->post() as $key => $val)
        {
            $check_parameters = ['username','password','mobile_no','otp_code','login_type','organization_id','branch_id','api_key','push_token','device_type','device_uuid','session_token'];
            if(!in_array($key, $check_parameters))
            {
                $requestStatus = 0;
                break;                
            }
            else{
                $requestStatus = 1;
            }  
        }
        if($requestStatus == 1)
        {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $mobile_no = $this->input->post('mobile_no'); 
            $branch_id = $this->input->post('branch_id');
            $organization_id = $this->input->post('organization_id');
            // $otp_code = $this->input->post('otp_code');
            // $login_type = $this->input->post('login_type');
            // $api_key = $this->input->post('api_key');
            // $push_token = $this->input->post('push_token');
            // $device_type = $this->input->post('device_type');
            // $device_uuid = $this->input->post('device_uuid');
            // $session_token = $this->input->post('session_token');
        
            $this->form_validation->set_rules('username','Username','trim|required');
            $this->form_validation->set_rules('password','Pasword','trim|required|min_length[6]|max_length[20]');
            $this->form_validation->set_rules('mobile_no','Mobile No','trim|regex_match[/^[0-9]{10}$/]');
            // $this->form_validation->set_rules('branch_id','Branch ID','trim|required');

            if($this->form_validation->run())
            {
                if (empty($branch_id) || empty($organization_id))
                {
                    $responseData = array(
                        'code'=> 200,
                        'status'=> false,
                        'message' => 'There is an issue with this login, please contact admin',
                        'data'=> (object)[]
                    );
                }
                else{
                    $data = array('username'=>$username,'password'=> $password,'mobile_no'=> $mobile_no,'branch_id'=> $branch_id,'organization_id'=> $organization_id);
                    $result = $this->api_model->checkLogin($data);
                    if(isset($result['user'])) 
                    {
                        $userId =  $result['user']->user_id;
                        $bearerToken = $this->api_auth->generateToken($userId);        
                        $responseData = array(
                            'code'=> 200,
                            'status'=> true,
                            'message' => 'Successfully Logged In',
                            'token'=> $bearerToken,
                            'data'=> $result
                        );                    
                    }
                    else if(isset($result['error']))
                    {
                        $responseData = array(
                            'code'=> 200,
                            'status'=> false,
                            'message' => $result['error'],
                            'data'=> (object)[]
                        );
                    }
                    else 
                    {
                        $responseData = array(
                            'code'=> 200,
                            'status'=>false,
                            'message' => 'Something went wrong, please try again',
                            'data'=> (object)[]
                        );
                    }
                }
            }
            else 
            {
                $responseData = array(
                    'code'=> 200,
                    'status'=>false,
                    'message' => array_values($this->form_validation->error_array())[0],
                    'data'=> (object)[]
                );
            }
        }
        else 
        {
            $responseData = array(
                'code'=> 200,
                'status'=>false,
                'message' => 'Invalid input parameters',
                'data'=> (object)[]
            );
        }
        return $this->response($responseData);
    }    

    public function userInfo()
    {
       $userId = $this->api_auth->getUserId();
       return $this->response($userId);
       if($userId){
            $result = $this->api_model->userData($userId);
            if(isset($result['error']))
            {
                $responseData = array(
                    'code'=> 200,
                    'status'=> false,
                    'message' => $result['error'],
                    'data'=> (object)[]
                );            
            }
            else 
            {
                $responseData = array(
                    'code'=> 200,
                    'status'=>true,
                    'message' => 'User details fetched successfully.',
                    'data'=> $result
                );
            }
       }
       else{
            $responseData = array(
                'code'=> 200,
                'status'=> false,
                'message' => $result['error'],
                'data'=> (object)[]
            ); 
       }
        return $this->response($responseData);
    }

    public function logout()
    {
        $result = $this->api_auth->logoutUser();
        if($result == true){            
            $responseData = array(
                'code'=> 200,
                'status'=> true,
                'message' => 'Successfully Logged Out'
            );            
        }
        else{
            $responseData = array(
                'code'=> 200,
                'status'=>false,
                'message' => 'Something went wrong, please try again.'
             );
        }
        return $this->response($responseData);
    }

}