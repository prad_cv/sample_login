<?php
class Api_Auth
{
    private $encryptKey = 'gdgdsnfg54n5t594g54gn4g04gj09j5gh043gh3403hj'; //random generated secret key
    private $secretIV = '5g8g5jg59jjhgh569538545t90yjyh04yj4y606ygfmhkgf'; //random generated initialization vector
    
    private $isTokenExpire=true; //set true for token expiry, false for never expire
    private $tokenExpriryInHours=24; //token expiry time in Hours

    private $encryptionMethod = "AES-256-CBC";
    private $output=null;

    public function encrypt($string)
    {
        $key = hash('sha256', $this->encryptKey);
        $iv = substr(hash('sha256', $this->secretIV), 0, 16);
        $encryptedText = openssl_encrypt($string, $this->encryptionMethod, $key, 0, $iv);
        return $encryptedText;
    }

    public function decrypt($string)
    {
        $key = hash('sha256', $this->encryptKey);
        $iv = substr(hash('sha256', $this->secretIV), 0, 16);
        $decryptedText = openssl_decrypt($string, $this->encryptionMethod, $key, 0, $iv);
        return $decryptedText;
    }

    public function generateToken($userId)
    {
        $tokenString = base64_encode(random_bytes(64));
        $token = strtr($tokenString, '+/', '-_');
        $mainToken = hash('sha256', $token);
        $this->storeTokenInAuthTokens($userId,$mainToken);
        $bearerToken =$mainToken.'.'.$this->encrypt($userId);
        return $bearerToken;
    }

    /* check user authentication */
    public function isNotAuthenticated()
    {
        $mainToken = $this->getMainToken();
        if($mainToken!=false)
        {
            $userId = $this->getUserId();
            $authStatus = $this->checkTokenFromUserTable($userId,$mainToken);
            if($authStatus==true)
            {
                return false;
            }
            else 
            {
                return true;
            }
        }
        else 
        {
            return false;
        }
       
    }


    /* get User Id */
    public function getUserId()
    {
        
        if($this->getTokenParts()!=false) 
        {
            $tokenPart = $this->getTokenParts();
            $userIdToken = $tokenPart[1];
            $userId = $this->decrypt($userIdToken);
            return $userId;
        }   
        else 
        {
            //echo 'Token parts Error!';exit;
            return false;
        }
    }

    /* get Token parts */
    public function getTokenParts()
    {        
        $bearerToken = $this->getBearerToken();        
        if($bearerToken==null) 
        {
            return false;
        }
        else 
        {
            $tokenChunks = explode(".", $bearerToken);
            $CI= &get_instance();  //Store instance in a variable.
            $CI->load->database();
            if($CI->db->query("SELECT * FROM bs_session WHERE session_token='$tokenChunks[0]'")->num_rows() > 0)
            {
                return $tokenChunks;
            }
            else{
                return false;
            }           
        }
    }


    /* Get Main Token */
    public function getMainToken()
    {   
        if($this->getTokenParts()!=false) 
        {
            $tokenPart = $this->getTokenParts();
            $mainToken = $tokenPart[0];
            
            return $mainToken;
        }   
        else 
        {
            return false;
        }
    }


    /** 
     * Get header Authorization
     * */
    function getAuthorizationHeader()
    {
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        } 
        else if (isset($_SERVER['HTTP_AUTHORIZATION'])) 
        { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } 
        elseif (function_exists('apache_request_headers')) 
        {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }

        return $headers;
    }

    /**
     * get access token from header
     * */
    function getBearerToken()
    {
        $headers = $this->getAuthorizationHeader();
        
        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }        
        return null;
    }

    function checkTokenFromUserTable($userId,$mainToken)
    {
        $CI= &get_instance();  //Store instance in a variable.
        $CI->load->database();
        $CI->db->select('*');
        $CI->db->from('bs_session');
        $CI->db->where(['user_id' => $userId,'session_token'=>$mainToken]);
        if($this->isTokenExpire)
        {
            $expiryDate = date('Y-m-d H:i:s');
            $CI->db->where('session_expire_time >=',$expiryDate);
        }
        $query = $CI->db->get();
        
        if($query->num_rows() == 0) 
        {
            return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    function storeTokenInAuthTokens($userId,$token)
    {
        $expirtyDate = $this->getTimeAfterHours($this->tokenExpriryInHours);
        $CI= &get_instance();  //Store instance in a variable.
        $CI->load->database();
        $CI->db->where(['user_id'=>$userId, 'session_token !='=>"",'session_expire_time >'=>date('Y-m-d H:i:s')]);
        $CI->db->update('bs_session',['session_token'=>"",'session_expire_time'=>date('Y-m-d H:i:s')]);
        $insert = array(
            'device_id'=>'1',
            'user_id'=> $userId,
            'push_token'=>'web',
            'session_token'=>$token,
            'last_access_time'=>date("Y-m-d H:i:s"),
            'login_type'=>'user',
            'session_expire_time'=>$expirtyDate
            );
        $CI->db->insert('bs_session',$insert); 
    }

    function getTimeAfterHours($hours)
    {
        if($hours!=null || $hours !=0)
        {
            return  date('Y-m-d H:i:s', strtotime($hours.' hour'));
        }
        else 
        {
            $hours=24; //setting default time
            return  date('Y-m-d H:i:s', strtotime($hours.' hour'));
        } 
    }

    public function logoutUser()
    {
        if($this->getTokenParts()!=false) 
        {
            $tokenPart = $this->getTokenParts();
            $userIdToken = $tokenPart[1];
            $userId = $this->decrypt($userIdToken);
            $update = array(
                'logout_time'=>date("Y-m-d H:i:s"),
                'last_access_time'=>date("Y-m-d H:i:s"),
                'session_expire_time'=>date("Y-m-d H:i:s"),
                'session_token'=>''
                );
            $CI= &get_instance();  //Store instance in a variable.
            $CI->load->database();           
            if($CI->db->get_where('bs_session',['user_id'=>$userId, 'session_token'=>$tokenPart[0]]))
            {
                $CI->db->where(['user_id'=>$userId, 'session_token'=>$tokenPart[0]]);
                $CI->db->update('bs_session',$update);
                return true;
            }         
            else{
                return false;
            }
        }   
        else 
        {
            return false;
        }
    }
    
}