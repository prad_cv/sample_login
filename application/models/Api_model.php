<?php 

class Api_model extends CI_Model 
{

    function checkLogin($data)
    {
        $username = $data['username'];
        $where = "(email='{$username}' or employee_id='{$username}')";
        $this->db->select('*');
        $this->db->from('bs_user');
        $this->db->where($where);      
        $query = $this->db->get();
        if($query->num_rows()==0)
        {
            return ['error'=>"User ID doesn't exist, please contact admin"];
        }
        else if($query->num_rows()==1)
        {
            $user = $query->row();

            // check active user
            if($user->is_inactive == 1 || $user->is_deleted == 1)
            {
                return ['error'=>"Your id is not enabled for access, please contact admin"];
            }

            // check duplicate mobile number
            $check_mobile = $this->db->get_where('bs_user',['mobile_no !=' => NULL,'mobile_no' => $user->mobile_no])->num_rows();
            if($check_mobile > 1)
            {
                return ['error'=>"There is another id with same mobile number, please contact admin"];
            }

            // check password
            if($user->password != $data['password'])
            {
                return ['error'=>"Invalid Credentials, please contact admin"];
            }

            // check duplicate user id
            $check_user = $this->db->get_where('bs_user',['employee_id' => $user->employee_id])->num_rows();
            if($check_user > 1)
            {
                return ['error'=>"There is an issue with this id, please contact admin"];
            }

            // check branch
            $check_branch = $this->db->get_where('bs_branch_user',['user_id' => $user->user_id, 'branch_id' => $data['branch_id']])->num_rows();
            if($check_user == 0)
            {
                return ['error'=>"Your access is limited, please contact admin"];
            }
            
            $this->db->select('b.*');
            $this->db->from('bs_branch b'); 
            $this->db->join('bs_branch_user bu', 'b.branch_id=bu.branch_id');
            $this->db->where(['b.organization_id'=>$data['organization_id']]);
            $this->db->where(['bu.user_id'=>$user->user_id,'bu.branch_id'=>$data['branch_id'],'bu.is_deleted'=>'0','bu.is_inactive'=>'0']);
            $branch_query = $this->db->get();
            if($branch_query->num_rows() > 0)
            {            
                $branch = $branch_query->result(); 
                return ['user'=>$user, 'branch'=>$branch];
            }
            else {
                return ['error'=>"Your access is limited, please contact admin"];
            }
        }
        else if($query->num_rows() > 1)
        {
            return ['error'=>"There is an issue with this id, please contact admin"];
        }
        else{
            return false;
        }
    }

    function userData($userId)
    {
        $this->db->select('*');
        $this->db->from('bs_user');
        $this->db->where(['user_id'=>$userId]);      
        $query = $this->db->get();
        if($query->num_rows()==0)
        {
            return ['error'=>"Unauthorized"];
        }
        else{
            return ['user'=>$query->row()];
        }
    }
}